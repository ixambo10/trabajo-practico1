package Interfaz;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import Calculadora.Calculo;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class Imagen extends JFrame {
	protected static final AbstractButton VisorCalc = null;

	private JPanel contentPane;
	private JTextField TextoEnElVisor;
	private StringBuilder Numero = new StringBuilder();
	private StringBuilder Mostrar = new StringBuilder();
	private Calculo Cau = new Calculo();
	private Boolean ContinuarCuenta = true;
	private Boolean primeroNumero = false;
	private JTextField textResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Imagen frame = new Imagen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	// Verifica que si se apret� el igual y despues se apreta un numero comienze de vuelta la cuenta
	public void verificaEstado() {

		if (!ContinuarCuenta) {
			reiniciaCalculadora();
		}

	}
    
	// Metodo que reinicia la calculadora a "Cero"
	public void reiniciaCalculadora() {
		Cau.InicializacionDeCalcualdora();
		Numero.setLength(0);
		Mostrar.setLength(0);
		TextoEnElVisor.setText("0");
		textResultado.setText("0");
		ContinuarCuenta = true;
	}

	// Carga los Numeros ingresados
	private void cargaNumero(ActionEvent e) {
		primeroNumero = true;
		JButton obj = (JButton) e.getSource();
		textResultado.setText(obj.getText() + " ");
		Numero.append(obj.getText());
		Mostrar.append(obj.getText());
	}
 
	// Muestra Numero parcial y operacion completa por pantalla.
	private void muestraNumero() {
		TextoEnElVisor.setText(Mostrar + " ");
		textResultado.setText(Numero + " ");
	}

	public Imagen() {
		setResizable(false);
		setTitle("Calculadora");
		setBackground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 385);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(3, 3, 3, 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Boton 1, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton1 = new JButton("1");
		Boton1.setForeground(SystemColor.desktop);
		Boton1.setFont(new Font("Arial", Font.BOLD, 14));
		Boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton1.setBounds(37, 104, 50, 50);
		contentPane.add(Boton1);

		// Boton 2, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton2 = new JButton("2");
		Boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton2.setFont(new Font("Arial", Font.BOLD, 14));
		Boton2.setBounds(97, 104, 50, 50);
		contentPane.add(Boton2);

		// Boton 3, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton3 = new JButton("3");
		Boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton3.setFont(new Font("Arial", Font.BOLD, 14));
		Boton3.setBounds(157, 104, 50, 50);
		contentPane.add(Boton3);

		// Boton 4, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton4 = new JButton("4");
		Boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton4.setFont(new Font("Arial", Font.BOLD, 14));
		Boton4.setBounds(37, 158, 50, 50);
		contentPane.add(Boton4);

		// Boton 5, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton5 = new JButton("5");
		Boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton5.setFont(new Font("Arial", Font.BOLD, 14));
		Boton5.setBounds(97, 158, 50, 50);
		contentPane.add(Boton5);

		// Boton 6, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton6 = new JButton("6");
		Boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton6.setFont(new Font("Arial", Font.BOLD, 14));
		Boton6.setBounds(157, 158, 50, 50);
		contentPane.add(Boton6);

		// Boton 7, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton7 = new JButton("7");
		Boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton7.setFont(new Font("Arial", Font.BOLD, 14));
		Boton7.setBounds(37, 212, 50, 50);
		contentPane.add(Boton7);

		// Boton 8, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton8 = new JButton("8");
		Boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton8.setFont(new Font("Arial", Font.BOLD, 14));
		Boton8.setBounds(97, 212, 50, 50);
		contentPane.add(Boton8);

		// Boton 9, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton9 = new JButton("9");
		Boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}
		});
		Boton9.setFont(new Font("Arial", Font.BOLD, 14));
		Boton9.setBounds(157, 212, 50, 50);
		contentPane.add(Boton9);

		// Boton 0, tres estados ( Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton Boton0 = new JButton("0");
		Boton0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaEstado();
				cargaNumero(e);
				muestraNumero();
			}

		});
		Boton0.setFont(new Font("Arial", Font.BOLD, 14));
		Boton0.setBounds(97, 266, 50, 50);
		contentPane.add(Boton0);

		// Boton Coma (  Verifica Estado / Carga el Numero / Lo Muestra por pantalla)
		JButton button_Coma = new JButton(".");
		button_Coma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					verificaEstado();
					cargaNumero(e);
					muestraNumero();
				}
			}
		});
		button_Coma.setFont(new Font("Arial", Font.BOLD, 14));
		button_Coma.setBounds(157, 266, 50, 50);
		contentPane.add(button_Coma);

		// Boton que cambia de signo al numero que ingreso o resultado parcial)
		JButton Boton_Signo = new JButton("\u00B1");
		Boton_Signo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					Cau.CargarOperaciones("-/+");
					Mostrar.insert(0, "-(");
					Mostrar.append(")");
					muestraNumero();
					ContinuarCuenta = true;
					primeroNumero = true;
				}
			}
		});
		Boton_Signo.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Signo.setBounds(37, 266, 50, 50);
		contentPane.add(Boton_Signo);

		// Funcion que realiza la multiplicacion de dos numeros
		JButton Boton_Multiplicar = new JButton("*");
		Boton_Multiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					Mostrar.append("*");
					Numero.setLength(0);
					String valorFinal = String.valueOf(Cau.CalcularTodo());
					textResultado.setText(valorFinal);
					TextoEnElVisor.setText(Mostrar.toString());
					ContinuarCuenta = true;
					Cau.CargarOperaciones("*");
					primeroNumero = false;
				}

			}
		});
		Boton_Multiplicar.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Multiplicar.setBounds(217, 158, 50, 50);
		contentPane.add(Boton_Multiplicar);

		// Funcion que realiza la Resta de dos numeros
		JButton Boton_Resta = new JButton("-");
		Boton_Resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					Mostrar.append("-");
					Numero.setLength(0);
					String valorFinal = String.valueOf(Cau.CalcularTodo());
					textResultado.setText(valorFinal);
					TextoEnElVisor.setText(Mostrar.toString());
					ContinuarCuenta = true;
					Cau.CargarOperaciones("-");
					primeroNumero = false;
				}
			}
		});
		Boton_Resta.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Resta.setBounds(217, 212, 50, 50);
		contentPane.add(Boton_Resta);

		// Funcion que realiza la Suma de dos numeros
		JButton Boton_Suma = new JButton("+");
		Boton_Suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					Mostrar.append("+");
					Numero.setLength(0);
					muestraNumero();
					ContinuarCuenta = true;
					Cau.CargarOperaciones("+");
					primeroNumero = false;
				}
			}
		});
		Boton_Suma.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Suma.setBounds(217, 266, 50, 50);
		contentPane.add(Boton_Suma);

		// Funcion que realiza la Division de dos numeros
		JButton Boton_Divisi�n = new JButton("\u00F7");
		Boton_Divisi�n.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					Mostrar.append("\u00F7");
					Numero.setLength(0);
					String valorFinal = String.valueOf(Cau.CalcularTodo());
					textResultado.setText(valorFinal);
					TextoEnElVisor.setText(Mostrar.toString());
					ContinuarCuenta = true;
					Cau.CargarOperaciones("\u00F7");
					primeroNumero = false;
				}
			}
		});
		Boton_Divisi�n.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Divisi�n.setBounds(217, 104, 50, 50);
		contentPane.add(Boton_Divisi�n);

		// Funcion Igual, da el resultado de la operaci�n cargada
		JButton Boton_Igual = new JButton("=");
		Boton_Igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (primeroNumero) {
					String contador = Numero.toString();
					double valor = Double.parseDouble(contador);
					Cau.CargarNumeros(valor);
					String valorFinal = String.valueOf(Cau.CargarOperaciones("="));
					TextoEnElVisor.setText(valorFinal);
					textResultado.setText("0");
					ContinuarCuenta = false;
					primeroNumero = false;
					primeroNumero = true;
				}
			}
		});
		Boton_Igual.setFont(new Font("Arial", Font.BOLD, 14));
		Boton_Igual.setBounds(291, 266, 50, 50);
		contentPane.add(Boton_Igual);
		
		//Funcion que limpia la cuenta parcial "C"
		JButton limpiaCuenta = new JButton("C");
		limpiaCuenta.setFont(new Font("Arial", Font.BOLD, 14));
		limpiaCuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cau.InicializacionDeCalcualdora();
				Numero.setLength(0);
				Mostrar.setLength(0);
				TextoEnElVisor.setText("0");
				textResultado.setText("0");
			}
		});
		limpiaCuenta.setBounds(291, 104, 50, 50);
		contentPane.add(limpiaCuenta);
		
		//Funcion que borra de a un numero de derecha a izquierda "DEL"
		JButton borraNumero = new JButton("Del");
		borraNumero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Numero.length() > 0) {
					Numero.deleteCharAt(Numero.length() - 1);
					Mostrar.deleteCharAt(Mostrar.length() - 1);
				}
				muestraNumero();
			}
		});
		borraNumero.setFont(new Font("Arial", Font.BOLD, 10));
		borraNumero.setBounds(291, 212, 50, 50);
		contentPane.add(borraNumero);

		//Funcion que limpia el numero parcial que se cargo "CE"
		JButton limpiaNumero = new JButton("CE");
		limpiaNumero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < Numero.length(); i++) {
					Mostrar.deleteCharAt(Mostrar.length() - 1);
				}
				Numero.setLength(0);
				muestraNumero();
			}
		});
		limpiaNumero.setFont(new Font("Arial", Font.BOLD, 11));
		limpiaNumero.setBounds(291, 158, 50, 50);
		contentPane.add(limpiaNumero);

		//Visualizacion del los resultados parciales
		textResultado = new JTextField();
		textResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		textResultado.setEditable(false);
		textResultado.setBounds(37, 60, 304, 33);
		contentPane.add(textResultado);
		textResultado.setColumns(10);

		//Visualizacion del numero parcial
		TextoEnElVisor = new JTextField();
		TextoEnElVisor.setHorizontalAlignment(SwingConstants.TRAILING);
		TextoEnElVisor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		TextoEnElVisor.setEditable(false);
		TextoEnElVisor.setText("");
		TextoEnElVisor.setBounds(37, 11, 304, 50);
		contentPane.add(TextoEnElVisor);
		TextoEnElVisor.setColumns(10);

	

	}
}
