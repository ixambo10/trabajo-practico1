package Calculadora;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculoTest {
	
	Calculo Cau = new Calculo ();
	BigDecimal Numero1,Numero2,Resultado;
	
	@BeforeEach
	public void setUp() {
		Numero1=new BigDecimal("4");
		Numero2=new BigDecimal("2");
		}
	
	// Test del Metodo Suma de la clase Calculo
	@Test 
	void Sumatest() {
		Cau.suma(Numero1, Numero2);
		Resultado=new BigDecimal("6");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	// Test del Metodo Resta de la clase Calculo
	@Test
	void Restatest() {
		Cau.resta(Numero1, Numero2);
		Resultado=new BigDecimal("2");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	// Test del Metodo Multiplicacion de la clase Calculo
	@Test
	void multiplicacion() {
		Cau.multiplicacion(Numero1, Numero2);
		Resultado=new BigDecimal("8");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	// Test del Metodo Division de la clase Calculo
	@Test
	void division() {
		Cau.division(Numero1, Numero2);
		Resultado=new BigDecimal("2.00");
		assertEquals(Cau.CalculoFinal,Resultado);
	}

	// Test del Metodo EstaOperacion de la clase Calculo
	@Test
	void EstaOperacion() {
		Cau.CargarOperaciones("+");
		String a = Cau.Operacion.get(0);
		assertEquals("+",a);
	}
	
	// Test del Metodo CalcularTodo de la clase Calculo
	@Test
	void CalcularTodo() {
		cargaDeNumerosOperaciones(Cau);
		Resultado=new BigDecimal("29.00");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	// Test del Metodo CambiarSimbolo de la clase Calculo
	@Test
	void CambiarSimbolo() {
		Cau.cambiarSigno(Numero1);
		Resultado=new BigDecimal("-4");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	// Test del Metodo SumaConComaTest de la clase Calculo
	@Test
	void SumaConComatest() {
		Numero1=new BigDecimal("2.2");
		Numero2=new BigDecimal("2.9");
		Cau.suma(Numero1, Numero2);
		Resultado=new BigDecimal("5.1");
		assertEquals(Cau.CalculoFinal,Resultado);
	}
	
	
	private void cargaDeNumerosOperaciones(Calculo Cau) {
		Cau.CargarNumeros(4);
		Cau.CargarNumeros(6);
		Cau.CargarNumeros(6);
		Cau.CargarNumeros(2);
		Cau.CargarNumeros(2);
		
		Cau.CargarOperaciones("+");
		Cau.CargarOperaciones("*");
		Cau.CargarOperaciones("-");
		Cau.CargarOperaciones("\u00F7");
		Cau.CargarOperaciones("=");
	}
	

}
