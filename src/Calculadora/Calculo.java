package Calculadora;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculo {

	ArrayList<BigDecimal> Numeros = new ArrayList<BigDecimal>();
	ArrayList<String> Operacion = new ArrayList<String>();
	BigDecimal CalculoFinal = new BigDecimal("0");

	// Inicializa la calculadora | Borra el contenido de los ArrayList
	public void InicializacionDeCalcualdora() {
		Numeros.clear();
		Operacion.clear();
	}

	// Carga los numeros ingresados en el ArrayList Numeros
	public void CargarNumeros(double Numero) {

		BigDecimal bigNumero = BigDecimal.valueOf(Numero);
		Numeros.add(bigNumero);

	}

	// Carga las operaciones ingresadas en el ArrayList Operaciones.
	public double CargarOperaciones(String Op) {
		if (Op.equals("=")) {
			Operacion.add(Op);
			CalcularTodo();
		} else {
			Operacion.add(Op);
		}

		Double CalculoFinalBig = CalculoFinal.doubleValue();

		return CalculoFinalBig;

	}

	// Con el contenido de los ArrayList, realiza el calculo solicitado
	public BigDecimal CalcularTodo() {

		CalculoFinal = Numeros.get(0);

		for (int i = 0; i < Operacion.size(); i++) {

			if (Operacion.get(i) == "+") {

				suma(CalculoFinal, Numeros.get(i + 1));

			}

			if (Operacion.get(i) == "-") {

				resta(CalculoFinal, Numeros.get(i + 1));
			}

			if (Operacion.get(i) == "*") {

				multiplicacion(CalculoFinal, Numeros.get(i + 1));
			}
			if (Operacion.get(i) == "÷") {

				division(CalculoFinal, Numeros.get(i + 1));
			}
			if (Operacion.get(i) == "-/+") {

				cambiarSigno(CalculoFinal);
			}

		}

		return CalculoFinal;
	}

	// Cambia el sigo de la operación.
	public void cambiarSigno(BigDecimal Numero) {

		CalculoFinal = Numero.negate();
	}

	// Resta numeros.
	public void resta(BigDecimal Numero_1, BigDecimal Numero_2) {
		CalculoFinal = Numero_1.subtract(Numero_2);

	}

	// Suma numeros
	public void suma(BigDecimal Numero_1, BigDecimal Numero_2) {
		CalculoFinal = Numero_1.add(Numero_2);
	}

	// Multiplica numeros
	public void multiplicacion(BigDecimal Numero_1, BigDecimal Numero_2) {

		MathContext returnRules = new MathContext(4, RoundingMode.HALF_DOWN);
		CalculoFinal = Numero_1.multiply(Numero_2,returnRules);
	}

	// Divide numeros
	public void division(BigDecimal Numero_1, BigDecimal Numero_2) {
		
		if(Numero_2.compareTo(new BigDecimal(0))== 0 ) {
			throw new IllegalArgumentException ("Realizo una cuanta que divide con Cero, FIN");	
		}
		else{
			CalculoFinal = (Numero_1.divide(Numero_2, 2, RoundingMode.CEILING));
		}
		
	}

}
